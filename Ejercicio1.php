<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div>
            <p>Escriba un número (0 > número <= 10) y dibujaréuna tabla de una columna de ese
                tamaño con cajas de texto en cada celda</p>

            <h3>Ejercicio 1</h3>
        </div>
        <div>
            <form method="get" action="ejer1Muestra.php">
                <label for="num">Tamaño de la tabla</label>
                <input type="number" name="num" id="num" required="" placeholder="Introduce un numero">

                <div>
                    <button type="submit" name="boton1">Dibujar</button>
                    <button type="reset">Restablecer</button>
                </div>
            </form>
        </div>
        
        <?php include './footer.php'; ?>
    </body>
</html>
